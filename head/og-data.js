module.exports = [
  {
    hid: 'og:title',
    vmid: 'og:title',
    name: 'og:title',
    content: 'Eventos Jaguar Land Rover',
  },
  {
    hid: 'og:type',
    property: 'og:type',
    vmid: 'og:type',
    content: 'website',
  },
  {
    hid: 'og:url',
    property: 'og:url',
    vmid: 'og:url',
    content: 'http://eventosjaguarlandrover.com/',
  },
  {
    hid: 'og:description',
    property: 'og:description',
    vmid: 'og:description',
    content: 'Celebrating our innovate connected tech',
  },
  {
    hid: 'og:image',
    property: 'og:image',
    vmid: 'og:image',
    content: 'http://eventosjaguarlandrover.com/',
  },
  {
    hid: 'twitter:card',
    property: 'twitter:card',
    vmid: 'twitter:card',
    content: 'summary',
  },
  {
    hid: 'twitter:site',
    property: 'twitter:site',
    vmid: 'twitter:site',
    content: '@JaguarCarsMx',
  },
  {
    hid: 'og:image:width',
    property: 'og:image:width',
    vmid: 'og:image:width',
    content: '1415',
  },
  {
    hid: 'og:image:height',
    property: 'og:image:height',
    vmid: 'og:image:height',
    content: '741',
  },
]
