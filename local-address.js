const os = require('os')

const port = process.env.PORT || 3000

const localAddress = () => {
  if (process.env.API_URL) {
    return process.env.API_URL
  }
  const ifaces = os.networkInterfaces()
  const h = os.hostname()
  if (h) {
    if (h.endsWith('.local')) {
      return `http://${h}`
    }
    return `http://${h}.local`
  }
  const result = []
  Object.keys(ifaces).forEach((ifname) => {
    let alias = 0
    ifaces[ifname].forEach((iface) => {
      if (iface.family !== 'IPv4' || iface.internal !== false) {
        // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
        return
      }
      if (alias >= 1) {
        // this single interface has multiple ipv4 addresses
        console.log(`Multiple addr: ${ifname}:${alias}`, iface.address)
        result.push(iface.address)
      } else {
        // this interface has only one ipv4 adress
        console.log(ifname, iface.address)
        result.push(iface.address)
      }
      // eslint-disable-next-line
      ++alias
    })
  })
  if (result.length > 1) {
    console.log('Hay múltiples direcciones posibles, probablemente sea una caja virtual')
  }
  console.log('Address:', `${localAddress()}:${port}`)
  return result[0]
}

module.exports = localAddress
